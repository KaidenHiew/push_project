<?php

namespace App;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Facades\FCM;

use Illuminate\Support\Facades\DB;

class Notification {

    function __construct() {
       
   }


    public function pushNotification() {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        // Database hit to get tokens
        \Log::info("\n\nSelect from device:");
        $tokens = DB::table('device')->pluck('Token')->toArray();
        
        $notificationBuilder = new PayloadNotificationBuilder('NOTIFICATION TITLE');
        $notificationBuilder->setBody('NOTIFICATION BODY')
                            ->setSound('default');
                            
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => 'SOME_DATA']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($tokens, $option, $notification);



        \Log::info("\n\nNumber of Successes:");
        \Log::info($downstreamResponse->numberSuccess());
        \Log::info("\nNumber of Failures:");
        \Log::info($downstreamResponse->numberFailure());
        \Log::info("\nNumber of Modifications:");
        \Log::info($downstreamResponse->numberModification());
        
        //return Array - you must remove all this tokens in your database
        \Log::info("\nTokens to Delete:");
        \Log::info($downstreamResponse->tokensToDelete());
        if($downstreamResponse->tokensToDelete())
            $this->deleteTokens($downstreamResponse->tokensToDelete());

        //return Array (key : oldToken, value : new token - you must change the token in your database )
        \Log::info("\nTokens to Modifys:");
        \Log::info($downstreamResponse->tokensToModify());
        


        //return Array - you should try to resend the message to the tokens in the array
        //This method either has some similarities to tokens to modify, or is in case of a send error

        \Log::info("\nTokens to Retry:");
        \Log::info($downstreamResponse->tokensToRetry());
        if($downstreamResponse->tokensToRetry())
            $this->retryTokens(tokensToRetry());
        \Log::info("\nToken Errors:");
        // return Array (key:token, value:errror) - in production you should remove from your database the tokens
        $downstreamResponse->tokensWithError(); 
    }

    /**
     * Deletes an array of tokens from DB.
     *
     * @param Request $request
     * @return Response
     */

     public function deleteTokens(Array $tokens)
    {
        
        //return Array - you must remove all this tokens in your database
        foreach($tokens as $token){
            \Log::info("Token deleted:");
            \Log::info($token);
            DB::table('device')->where('Token', '=', $token)->delete();
        }
        /*
        $request object populated by the send Notifcation
        function called if tokensToDelete is not Null
        Code for removing all the tokens from the DB that the FCM says are no longer valid.
        Deletes all tokens from DB that match those in the array
        */
    }

    /**
     * Updates an array of tokens from DB. The return values from tokensToModify doesn't seem to work properly with the 
     * tester function, will need practical testing
     * @param Request $request
     * @return Response
     */

     public function updateTokens(Array $updates)
    {
        
        //return Array (key : oldToken, value : new token - you must change the token in your database )
        foreach($updates as $token){
            \Log::info("Token updated:");
            \Log::info($token);
            $result = DB::update('update device set Token = ? where Token = ?', ['', 'Old']);
            \Log::info($result);
        }
        /*
        $request object populated by the send Notifcation
        function called if tokensToModify is not Null
        Code for replacing old tokens in the DB with new tokens
        inserts new token values in place of old tokens
        */
    }

    /**
     * Resends Tokens.
     *
     * @param Array $tokens
     * @return Response
     */

     public function retryTokens(Array $tokens)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder('NOTIFICATION TITLE');
        $notificationBuilder->setBody('NOTIFICATION BODY')
                            ->setSound('default');
                            
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(['a_data' => 'SOME_DATA']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($tokens, $option, $notification);


        \Log::info("\n\nNumber of Successes:");
        \Log::info($downstreamResponse->numberSuccess());
        \Log::info("\nNumber of Failures:");
        \Log::info($downstreamResponse->numberFailure());
        \Log::info("\nNumber of Modifications:");
        \Log::info($downstreamResponse->numberModification());
        
        //Resends notifications to an array of tokens returned by tokensToRetry
    }

    

}