<?php

namespace App;

use Illuminate\Notifications\Notifiable;

class Device
{
    use Notifiable;

    const CREATED_AT = 'CreatedOnUtc';
    const UPDATED_AT = 'UpdatedOnUtc';

    
    protected $primaryKey = 'ID';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token', 'userID',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    /*protected $hidden = [
        'password', 'remember_token',
    ];*/
}
