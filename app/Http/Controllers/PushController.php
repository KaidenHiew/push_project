<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use App\Notification;
use Illuminate\Http\Request;

use Carbon\Carbon;

use Illuminate\Support\Facades\DB;


class PushController extends Controller
{


    /**
     * Sends a push notification.
     *
     * @param
     * @return Response
     */
    public function sendNotification()
    {
        $notification = new Notification();
        $notification->pushNotification();  
    }
    
    /**
     * Sends a push notification.
     *
     * @param Request request
     * @return Response
     */
    public function insertToken(Request $request)
    {
        \Log::info("This is the request");
        $this->validate($request, [
        'token' => 'required|max:200',
        'userid' => 'required',
        ]);
        \Log::info($request);
        $myTime = Carbon::now();
        
        DB::insert('insert into device (Token, UserID, CreatedOnUTC) values (?, ?, ?)', [$request->token, $request->userid, $myTime]);
    }

    
    public function legacyEmail(Request $request)
    {
        $this->db->where('SendTime < NOW()');
        $this->db->join('user_moodcheck_schedule', 'user_moodcheck_schedule.ID = today_moodcheck_schedule.UserMoodcheckScheduleID');
        $result = $this->db->get('today_moodcheck_schedule');

        if ($result->num_rows()) {
            $result = $result->result();

            foreach ($result as $scheduleentry) {
                $user = $this->user_model->get($scheduleentry->UserID);
                $preferences = $this->user_model->getPreferences($scheduleentry->UserID);
                $hasRecentMoodcheck = $this->user_activity_model->UserhasRecentMoodcheck($scheduleentry->UserID, NUM_DAYS_STALE_MOODCHECK);
                if ($preferences->MoodcheckScheduleEmails == 1) {
                if ($hasRecentMoodcheck || (strtotime($user->CreatedOnUtc) >= (time() - (NUM_DAYS_STALE_MOODCHECK * 60 * 60 * 24)))) {
                    $this->emailsender->sendMoodcheckEmail($user);
                }
                else {
                    //sendMoodcheckOffEmail($user->Email);
                    //$this->user_model->update_preference($user->ID, 'MoodcheckScheduleEmails', 0);
                }
                }
                $this->db->delete('today_moodcheck_schedule', array(
                'UserMoodcheckScheduleID' => $scheduleentry->ID,
                'SendTime' => $scheduleentry->SendTime
                ));
            }
        }
    }


}