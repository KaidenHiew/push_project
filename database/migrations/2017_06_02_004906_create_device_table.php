<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeviceTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('device', function(Blueprint $table) {
      $table->increments('ID');
      $table->string('Token', 200)->nullable();
      $table->integer('UserID')->nullable()->index('UserID');
      $table->dateTime('CreatedOnUtc')->nullable();
      $table->timestamp('UpdatedOnUtc')->default(DB::raw('CURRENT_TIMESTAMP'));
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('device');
  }

}
